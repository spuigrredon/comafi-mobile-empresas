  Run instructions for iOS:
    • cd "C:\Users\herna\Desktop\comafiMobileEmpresas" && npx react-native run-ios
    - or -
    • Open comafiMobileEmpresas\ios\comafiMobileEmpresas.xcodeproj in Xcode or run "xed -b ios"
    • Hit the Run button

  Run instructions for Android:
    • Have an Android emulator running (quickest way to get started), or a device connected.
    • cd "C:\Users\herna\Desktop\comafiMobileEmpresas" && npx react-native run-android

  Run instructions for Windows and macOS:
    • See https://aka.ms/ReactNative for the latest up-to-date instructions.